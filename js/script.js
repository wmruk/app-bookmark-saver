document.getElementById('myForm').addEventListener('submit', saveLinks);

function checkUrl(link) {
    var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    var regex = new RegExp(expression);
    if (!link.match(regex)) {
        return false;
    }

    return true;
}

function createHtml(links) {
    $('#results').html('');
    links.forEach(function (element, index) {
        var html = '<div class="col alert alert-info" role="alert">' +
                '<h3>' + element.name + '</h3>' +
                '<a href="' + element.url + '" target="_blank">' + '<button type="button" class="btn btn-success">Visit the page</button>' + '</a>' +
                '<button type="button" class="btn btn-danger  delete" data-delete="' + index + '">Delete the page</button>' +
                '</div>'
        $('#results').append(html);
    });
}

function createLink(link, links) {
    links.push(link);
    localStorage.setItem("savedLinks", JSON.stringify(links));
    createHtml(links);
}

function saveLinks(e) {
    e.preventDefault();
    var link = {
        name: document.getElementById('siteName').value,
        url: document.getElementById('siteUrl').value
    };
    if (link.name === '' || link.url === '') {
        alert('Name and url can\'t be empty');
        return false;
    }

    if (!checkUrl(link.url)) {
        alert("Provide correct url starting with HTTP:// or HTTPS://");

        return false;
    }

    links = localStorage.getItem("savedLinks") === null ? [] : JSON.parse(localStorage.getItem('savedLinks'));
    createLink(link, links);
}

$(document).on('click', '.delete', function (e) {
    var id = $(this).data('delete');
    links = JSON.parse(localStorage.getItem('savedLinks'));
    links.splice(id, 1);
    localStorage.setItem("savedLinks", JSON.stringify(links));
    $(this).parent().hide();
    createHtml(links);
});

$(document).ready(function () {
    links = localStorage.getItem("savedLinks") === null ? [] : JSON.parse(localStorage.getItem('savedLinks'));
    createHtml(links);
});

$(document).ready(function () {
    var random_bg = Math.round(Math.random(5) * 4) + 1;
    $(document.body).css({'background-image': 'url(backgrounds/' + random_bg + 'a.jpg)'});
});